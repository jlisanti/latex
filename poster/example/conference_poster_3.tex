%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% baposter Portrait Poster
% LaTeX Template
% Version 1.0 (15/5/13)
%
% Created by:
% Brian Amberg (baposter@brian-amberg.de)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0paper,portrait]{baposter}

\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{relsize} % Used for making text smaller in some places
\usepackage[style=alphabetic,doi=false,url=false,isbn=false,natbib=true]
   {biblatex}

\graphicspath{{figures/}} % Directory in which figures are stored

\definecolor{bordercol}{RGB}{255,255,255} % Border color of content boxes
%\definecolor{headercol1}{RGB}{128,0,0} % Background color for the header in the content boxes (left side)
\definecolor{headercol1}{RGB}{158,36,68} % Background color for the header in the content boxes (right side)

\definecolor{headercol2}{RGB}{158,36,68} % Background color for the header in the content boxes (right side)
\definecolor{headerfontcol}{RGB}{255,255,255} % Text color for the header text in the content boxes
\definecolor{boxcolor}{RGB}{255,255,255}% Background color for the content in the content boxes

\usepackage{eso-pic}
\newcommand\BackgroundPic{
\put(0,0){
\parbox[b][\paperheight]{\paperwidth}{%
\vfill
\centering
\includegraphics[width=\paperwidth,height=\paperheight,
keepaspectratio]{backgroundMod.png}%
\vfill
}}}

\begin{document}
%\AddToShipoutPicture{\BackgroundPic}

\background{ % Set the background to an image (background.pdf)
%\begin{tikzpicture}[remember picture,overlay]
%\draw (current page.north west)+(-2em,2em) node[anchor=north west]
%\includegraphics[height=1.1\textheight]{background};
%\end{tikzpicture}
}

\begin{poster}{
grid=false,
borderColor=bordercol, % Border color of content boxes
headerColorOne=headercol1, % Background color for the header in the content boxes (left side)
headerColorTwo=headercol2, % Background color for the header in the content boxes (right side)
headerFontColor=headerfontcol, % Text color for the header text in the content boxes
boxColorOne=boxcolor, % Background color for the content in the content boxes
%headershape=roundedright, % Specify the rounded corner in the content box headers
headerfont=\Large\sf\bf, % Font modifiers for the text in the content box headers
textborder=rectangle,
background=user,
headerborder=closed, % Change to closed for a line under the content box headers
boxshade=plain
}
{}
%
%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR NAME
%----------------------------------------------------------------------------------------
%
%{\sf\bf Acoustically modulated pressure gain combustor for application to isochoric heat addition gas turbines} % Poster title
{ Acoustically Modulated Pressure Gain Combustor for Application to Isochoric Heat Addition Gas Turbines} 
{\vspace{.0em} }%John Smith, James Smith, Jane Smith\\ }% Author names
{\includegraphics[scale=0.28]{figures/KaustLogo.jpg}} % University/lab logo


%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\headerbox{Introduction}{name=introduction,column=0,row=0}{
After nearly 80 years of development, only incremental gains in conventional gas turbine engine efficiency are available at significant cost. However, accounting for nearly 10\% of global energy consumption, there remains great interest in producing further performance improvements \\

It is known that the largest loss of thermodynamic availability in a gas turbine engine occurs within the combustion chamber.  Gas turbine operation is described thermodynamically by the Brayton cycle, which suffers from a loss in total pressure across the combustor, thereby greatly reducing the efficiency of the cycle.\\

The net loss in total pressure may be replaced by a net gain if the isobaric heat addition process could be replaced by isochoric heat addition.  Such a combustor is known as a constant volume or pressure gain combustor.

}

%----------------------------------------------------------------------------------------
%	FOOTER BOX
%----------------------------------------------------------------------------------------

\headerbox{}{name=footer,column=0,span=3,above=bottom,textborder=none,headerborder=none,boxheaderheight=0pt}{ 
\begin{center}
\includegraphics[width=1.0\linewidth]{footer}
%\captionof{figure}{Figure caption}
\end{center}
}

%----------------------------------------------------------------------------------------
%	APPROACH
%----------------------------------------------------------------------------------------

\headerbox{Approach}{name=methods,column=0,below=introduction}{
For the purpose of studying pressure gain combustion at elevated pressure, an acoustically modulated pulsed combustor has been developed.  This combustor is capable of operating on the unsteady Humphrey thermodynamic cycle and is coupled with an unsteady ejector. 

\begin{center}
\includegraphics[width=1.0\linewidth]{pc}
\captionof{figure}{Experimental pulsed combustor}
\end{center}

A robust active valve has been developed to decouple the combustor valve motion from the complexities of the unsteady combustion process.  This 40 cm valved pulsed combustor will operate at ~510 Hz and a mechanically coupled fuel valve will allow for precise control of the phase and pulse width of fuel injection.


}

%----------------------------------------------------------------------------------------
%	OBJECTIVE
%----------------------------------------------------------------------------------------

\headerbox{Objective}{name=conclusion,column=0,below=methods}{

No experimental data has been obtained for a Pulsed Combustor Ejector Shroud (PCES) system operating in gas turbine relevant conditions (i.e. elevated pressure).  Also, little experimental data exists detailing PCES pollutant emissions (NOx, CO, UHC), none at relevant pressures. For this reason, the objectives are:
\begin{enumerate}
\item Replicated and compare with previous low pressure experimental studies
\item Characterize PCES system performance
\item Demonstrate net total pressure gain across PCES at 10 bar with liquid transportation fuels
\end{enumerate}
}

%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

%\headerbox{References}{name=references,column=0,below=conclusion}{

%\smaller % Reduce the font size in this block
%\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
%\nocite{*} % Insert publications even if they are not cited in the poster

%\bibliographystyle{unsrt}
%\bibliography{sample} % Use sample.bib as the bibliography file
%}

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

%\headerbox{Acknowledgements}{name=acknowledgements,column=0,below=references, above=bottom}{

%\smaller % Reduce the font size in this block
%The project is funded by the KAUST Office of Competitive Research and Clean Combustion Research Center.
%} 

%----------------------------------------------------------------------------------------
%	BACKGROUND
%----------------------------------------------------------------------------------------

\headerbox{Background}{name=results1,span=2,column=1,row=0}{ % To reduce this block to 1 column width, remove 'span=2'
Previous studies have addressed key issues with isochoric combustors (inherent unsteady flow and bypass ratio) and demonstrated that a meaningful pressure gain may be achieved.

\begin{center}
\includegraphics[width=0.9\linewidth]{cycle_comparison}
\captionof{figure}{Increase in gas turbine efficiency achieved through isochoric heat addition, \cite{Ward and Miller, 2012}}
\end{center}

State of the Art
\begin{itemize}
\item Kentfield and Fernades (1990) integrated pressure-gain combustor into a small gas turbine engine and produced a pressure gain of of 4\% of the absolute delivery pressure. 
\item Paxson and Dougherty (2005) demonstrated an overall pressure gain of 3.5\% with a pulsed combustor-ejector-shroud system at overall temperature ratios relevant to gas turbine applications.
\item Heffer et al. (2008) found that the velocity fluctuations at the exit plane of a typical ejector are of the order of 4\% of the magnitude of velocity fluctuations occurring at the combustor exit, an acceptable level for turbine operation.
\item Offord et al. (2008) demonstrated that unsteady fuel injection may be used to raise the efficiency of a pulsed combustor by factors ranging from 7 to 2 at low and high fuel mass flow rates respectively.
\end{itemize}
%------------------------------------------------

%\begin{center}
%\begin{tabular}{l l l}
%\toprule
%\textbf{Treatments} & \textbf{Response 1} & \textbf{Response 2}\\
%\midrule
%Treatment 1 & 0.0003262 & 0.562 \\
%Treatment 2 & 0.0015681 & 0.910 \\
%Treatment 3 & 0.0009271 & 0.296 \\
%\bottomrule
%\end{tabular}
%\captionof{table}{Table caption}
%\end{center}
}

%----------------------------------------------------------------------------------------
%	HIGH PRESSURE APPARATUS
%----------------------------------------------------------------------------------------

\headerbox{High Pressure Apparatus}{name=results2,span=2,column=1,below=results1,above=bottom,above=footer}{ % To reduce this block to 1 column width, remove 'span=2'

%------------------------------------------------

%\begin{center}
%\includegraphics[width=0.49\linewidth]{placeholder}
%\includegraphics[width=0.49\linewidth]{placeholder}
%\captionof{figure}{Figure caption 1 (left); Figure caption 2 (right)}
%\end{center}

%------------------------------------------------
High pressure apparatus will allow for continuous operation at 10 bar and gas turbine relevant conditions.
System performance will be characterized through measurement of the total pressure ratio across the combustor as a function of the total temperature ratio.
%------------------------------------------------

\begin{center}
\includegraphics[width=1.\linewidth]{Experiment_Assembly_Top_View}
%\captionof{figure}{Figure caption}
\end{center}
\begin{center}
\includegraphics[width=1.\linewidth]{Experiment_Assembly_Side_View}
%\captionof{figure}{Figure caption}
\end{center}

%------------------------------------------------
Emissions will be measured through a combination of in-situ probe measurements and time resolved NO Planar Laser Induced Fluorescence.
Once system operation is validated and performance/emissions have been characterized for gaseous fuels, the apparatus will be modified for operation with liquid transportation fuels. 
}

%----------------------------------------------------------------------------------------

\end{poster}

\end{document}