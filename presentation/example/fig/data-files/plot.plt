set key top left
set yrange[-0.3:1.]
set arrow 1 from (1./508.),-0.3 to (1./508.),1.0 nohead lt 4
set label 1 "Cycle 1" at (1./508.),1.05 center
set arrow 2 from (1.5/508.),-0.3 to (1.5/508.),1.0 nohead lt 5 
set label 2 "Cycle 1.5" at (1.5/508.),1.05 center
set arrow 3 from (2./508.),-0.3 to (2./508.),1.0 nohead lt 4
set label 3 "Cycle 2" at (2./508.),1.05 center
set arrow 4 from (2.5/508.),-0.3 to (2.5/508.),1.0 nohead lt 5 
set label 4 "Cycle 2.5" at (2.5/508.),1.05 center
set arrow 5 from (3./508.),-0.3 to (3./508.),1.0 nohead lt 4
set label 5 "Cycle 3." at (3./508.),1.05 center
set arrow 6 from (3.5/508.),-0.3 to (3.5/508.),1.0 nohead lt 5 
set label 6 "Cycle 3.5" at (3.5/508.), 1.055 center
plot "combustor-pressure.dat" u 1:(($2-101325)/50000) axes x1y1 w l lt 1,\
     "combustor-inlet-flow-rate" u 1:2 axes x1y2 w l lt 2,\
     "dT.dat" u 1:($2/15) every 10 axes x1y1 w p lt 6,\
     "fuel-flow-rate.dat" u 1:2 axes x1y2 w l lt 3
pause 1
reread
