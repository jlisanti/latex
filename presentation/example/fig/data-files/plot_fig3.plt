#!/bin/gnuplot
set term post enhanced eps "Times,14" solid
set pointsize 1.5
set autoscale y
set y2range [0.0:0.01]
set y2label "Mass Flow Rate [kg/s]"
set ylabel "Pressure"
set xlabel "Time [seconds]"
set ytics nomirror
set y2tics nomirror
set out "fig3.eps"
set termoption dashed
set key top left
set yrange[-0.3:1.]

#Vertical Lines
#set arrow 1 from (1./508.),-0.3 to (1./508.),0.7 nohead lt 4
#set label 1 "Cycle 1" at (1./508.),0.75 center
#set arrow 2 from (1.5/508.),-0.3 to (1.5/508.),1.0 nohead lt 5 
#set label 2 "Cycle 1.5" at (1.5/508.),1.05 center
#set arrow 3 from (2./508.),-0.3 to (2./508.),0.7 nohead lt 4
#set label 3 "Cycle 2" at (2./508.),.75 center
#set arrow 4 from (2.5/508.),-0.3 to (2.5/508.),1.0 nohead lt 5 
#set label 4 "Cycle 2.5" at (2.5/508.),1.05 center
#set arrow 5 from (3./508.),-0.3 to (3./508.),0.7 nohead lt 4
#set label 5 "Cycle 3" at (3./508.),0.75 center
#set arrow 6 from (3.5/508.),-0.3 to (3.5/508.),1.0 nohead lt 5 
#set label 6 "Cycle 3.5" at (3.5/508.), 1.055 center

#set arrow 6 from (4./508.),-0.3 to (4./508.),0.7 nohead lt 4
#set label 6 "Cycle 4" at (4./508.),0.75 center

#set arrow 11 from 0.003641,-0.3 to 0.003641,.6 nohead lt 5 
#set label 11 "Cycle 3.5" at 0.003641, 0.85 center

#set arrow 12 from 0.002719,-0.3 to 0.002719,0.6 nohead lt 5

#set arrow 13 from 0.00657,-0.3 to 0.00657,0.6 nohead lt 5 
#set label 12 "Cycle 3.5" at (3.5/508.), 1.055 center

#set arrow 14 from 0.008556,-0.3 to 0.008556,0.6 nohead lt 5 
#set label 12 "Cycle 3.5" at (3.5/508.), 1.055 center

#set arrow 15 from 0.0046175,-0.3 to 0.0046175,0.6 nohead lt 5

set arrow 16 from 0.003,0.63 to 0.0012,0.18  head size screen 0.01,45
set label 16 "Energy injection" at 0.003,0.67 center
#Horizontal lines
#First peak
#set arrow 7 from 0.002719,0.6 to 0.0046175,0.6 heads size screen 0.01,90
#set label 7 "{/Symbol D}t=1.90 ms" at 0.00366825,0.7 center
#Second peak
#set arrow 8 from 0.0046175,0.6 to 0.00657,0.6 heads size screen 0.01,90
#set label 8 "{/Symbol D}t=1.95 ms" at 0.00559375,0.7 center
#Third peak
#set arrow 9 from 0.00657,0.6 to 0.008556,0.6 heads size screen 0.01,90
#set label 9 "{/Symbol D}t=1.98 ms" at 0.007563,0.7 center

set object 10 circle at 0.00082,0.08 size scr 0.05 fc rgb "red"

#plot "combustor-pressure.dat" u 1:(($2-101325)/50000) axes x1y1 w l lt 1 title "P-P_{b}/P_{o}",\
#     "combustor-inlet-flow-rate" u 1:2 axes x1y2 w l lt 2 title "Combustor Inlet Mass Flow Rate",\
#     "dT.dat" u 1:($2/15) smooth csplines axes x1y1 w l lt 6 title "Positive dT",\
#     "fuel-flow-rate.dat" u 1:2 axes x1y2 w l lt 3 title "Fuel Mass Flow Rate",\
#     1  axes x1y1 w l notitle
plot "combustor-pressure.dat" u 1:(($2-101325)/50000) axes x1y1 w l lt 1 lw 0.7 title "P-P_{b}/P_{o}",\
     "combustor-inlet-flow-rate" u 1:2 axes x1y2 w l lt 2 lw .7 title "Combustor Inlet Mass Flow Rate",\
     "fuel-flow-rate.dat" u 1:2 axes x1y2 w l lt 4 lw 3.0 title "Fuel Mass Flow Rate",\
     1  axes x1y1 w l notitle


#plot "combustor-pressure.dat" u 1:(($2-101325)/50000) axes x1y1 w l lt 1 title "P-P_{b}/P_{o}",\
#     "fuel-flow-rate.dat" u 1:2 axes x1y2 w l lt 3 title "Fuel Mass Flow Rate",\
#     1  axes x1y1 w l notitle
