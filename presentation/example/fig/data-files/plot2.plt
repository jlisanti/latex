plot "combustor-pressure.dat" u 1:($2/101325) axes x1y1 w l lt 1,\
     "combustor-temperature.dat" u 1:($2/1500) axes x1y2 w l lt 4
pause 1
reread
