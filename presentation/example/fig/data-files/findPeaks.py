import numpy as np

f = open('maxmin.dat','w')
data=[]
data3=[]
with open('combustor-pressure.dat','r') as inputf:
    next(inputf)
    next(inputf)
    for line in inputf:
        val = [float(x) for x in line.split()]
        data.append(val[1])
        data3.append(val)
data2 = np.array(data)

a=np.array(data,dtype=np.float)

gradients=np.diff(a)
print gradients

maxima_num=0
minima_num=0
max_locations=[]
min_locations=[]
count=0
for i in gradients[:-1]:
    count+=1

    if ((cmp(i,0)>0) & (cmp(gradients[count],0)<0) & (i != gradients[count])):
        maxima_num+=1
        max_locations.append(count)     

    if ((cmp(i,0)<0) & (cmp(gradients[count],0)>0) & (i != gradients[count])):
        minima_num+=1
        min_locations.append(count)


turning_points = {'maxima_number':maxima_num,'minima_number':minima_num,'maxima_locations':max_locations,'minima_locations':min_locations}  
for i in range(0,maxima_num):
    if data3[max_locations[i]][1] > 100000.:
        print data3[max_locations[i]][0]

print data3[5543][0]
print turning_points
